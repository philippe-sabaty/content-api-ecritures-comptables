.DEFAULT_GOAL := help
#.SILENT:
#.PHONY: vendor build front

DCC=docker compose
API=$(DCC) exec php

include api/.env
-include api/.env.local
export

help:   ## Show this help.
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'|sort

api-install:
	$(API) composer install

api-shell:
	$(API) sh

api-build-entities:
	$(API) vendor/bin/schema generate src config/schema.yaml  -vv

api-schema-generate:
	$(API) php bin/console api:openapi:export > spec/schema.generated.json
