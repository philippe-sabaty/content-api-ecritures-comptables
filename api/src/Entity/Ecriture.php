<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
class Ecriture
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    public ?int $id = null;

    /**
     * pour alimenter internalNumber=NUIOCECT / C'est un numéro provisoire, commun pour une même écriture et qui ne sera pas conservé lors de l'intégration dans les différentes tables.
     */
    #[ORM\Column(type: 'text')]
    #[Assert\NotNull]
    public string $NumeroDecriture;

    /**
     * pour alimenter transactionType=TYPOCECT / doit exister dans GPAR PARAM=TYPMVC.
     */
    #[ORM\Column(type: 'text')]
    #[Assert\NotNull]
    public string $TypeDecriture;

    /**
     * pour alimenter entity=ETSOCECT / doit exister dans GETS Intitulé complet INTGTETS.
     */
    #[ORM\Column(type: 'text')]
    #[Assert\NotNull]
    public string $Etablissement;

    /**
     * pour alimenter journal=JRNOCECT / doit exister dans GJRN OEJRN.NUMOEJRN.
     */
    #[ORM\Column(type: 'text', name: '`Journal`')]
    #[Assert\NotNull]
    public string $Journal;

    /**
     * pour alimenter voucher=PIEOCECT /Si le journal est en numérotation automatique, ne rien renseigner / sinon fournir le numéro de la pièce (libre 10 max).
     */
    #[ORM\Column(type: 'text')]
    #[Assert\NotNull]
    public string $NumeroPiece;

    /**
     * pour alimenter transactionType=TYPOCECT / doit exister dans GPAR PARAM=TYPPIE.
     */
    #[ORM\Column(type: 'text')]
    #[Assert\NotNull]
    public string $TypeDePiece;

    /**
     * pour alimenter accountingDate=DECOCECT.
     */
    #[ORM\Column(type: 'text')]
    #[Assert\NotNull]
    public string $DateComptable;

    /**
     * pour alimenter issueDate=DEMOCECT.
     */
    #[ORM\Column(type: 'text')]
    #[Assert\NotNull]
    public string $DateEmission;

    /**
     * pour alimenter endDate=ECHOCECT.
     */
    #[ORM\Column(type: 'text')]
    #[Assert\NotNull]
    public string $DateEcheance;

    /**
     * pour alimenter paymentMethod=RGMOCECT / doit exister dans GRGM Intitulé complet INTOERGM.
     */
    #[ORM\Column(type: 'text')]
    #[Assert\NotNull]
    public string $ModeDePaiement;

    /**
     * pour alimenter description=LIBOCECT.
     */
    #[ORM\Column(type: 'text')]
    #[Assert\NotNull]
    public string $Libelle;

    /**
     * pour alimenter description=LIBOCECT.
     */
    #[ORM\Column(type: 'text')]
    #[Assert\NotNull]
    public string $LibelleComplementaire;

    /**
     * pour alimenter additionalDescription=LICOCECT.
     */
    #[ORM\Column(type: 'text')]
    #[Assert\NotNull]
    public string $LibelleComplementaire2;

    /**
     * pour alimenter currency=DEVOCECT.
     */
    #[ORM\Column(type: 'text')]
    #[Assert\NotNull]
    public string $Devise;

    /**
     * pour alimenter note=BOROCECT.
     */
    #[ORM\Column(type: 'text')]
    #[Assert\NotNull]
    public string $Bordereau;

    /**
     * pour alimenter nature=NATOCECT.
     */
    #[ORM\Column(type: 'text')]
    #[Assert\NotNull]
    public string $NatureEcriture;

    /**
     * pour alimenter kind=GENOCECT.
     */
    #[ORM\Column(type: 'text')]
    #[Assert\NotNull]
    public string $GenreEcriture;

    /**
     * pour alimenter role=ROLOCECT.
     */
    #[ORM\Column(type: 'text')]
    #[Assert\NotNull]
    public string $RoleEcriture;

    /**
     * pour alimenter voucherNature=NAPOCECT.
     */
    #[ORM\Column(type: 'text')]
    #[Assert\NotNull]
    public string $NaturePiece;

    /**
     * pour alimenter voucherKind=GEPOCECT.
     */
    #[ORM\Column(type: 'text')]
    #[Assert\NotNull]
    public string $GenrePiece;

    /**
     * pour alimenter voucherRole=ROPOCECT.
     */
    #[ORM\Column(type: 'text')]
    #[Assert\NotNull]
    public string $RolePiece;

    #[ORM\Column(type: 'json')]
    #[Assert\NotNull]
    public array $Tiers = [];

    /**
     * pour alimenter entryFile=DOEOCECT.
     */
    #[ORM\Column(type: 'text')]
    #[Assert\NotNull]
    public string $Dossier;

    #[Assert\NotNull]
    public string $Createur;

    /**
     * pour alimenter creationDate=DCROCECT.
     */
    #[ORM\Column(type: 'text')]
    #[Assert\NotNull]
    public string $DateCreation;

    #[ORM\Column(type: 'json')]
    #[Assert\NotNull]
    public array $Metadonnees = [];

    #[ORM\Column(type: 'json')]
    #[Assert\NotNull]
    public array $Lignes = [];

    public function getId(): ?int
    {
        return $this->id;
    }

}
