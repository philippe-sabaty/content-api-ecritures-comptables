<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Get;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * This is a dummy entity. Remove it!
 */
#[ORM\Entity]
class EcritureLight
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    /**
     * pour alimenter internalNumber=NUIOCECT / C'est un numéro provisoire, commun pour une même écriture et qui ne sera pas conservé lors de l'intégration dans les différentes tables.
     */
    #[ORM\Column(type: 'text')]
    #[Assert\NotNull]
    private string $NumeroDecriture;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): EcritureLight
    {
        $this->id = $id;
        return $this;
    }

    public function getNumeroDecriture(): string
    {
        return $this->NumeroDecriture;
    }

    public function setNumeroDecriture(string $NumeroDecriture): EcritureLight
    {
        $this->NumeroDecriture = $NumeroDecriture;
        return $this;
    }

}
