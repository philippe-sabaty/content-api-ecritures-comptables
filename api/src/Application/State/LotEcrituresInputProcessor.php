<?php

namespace App\Application\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\Domain\Model\Additi\LotEcritures;

/**
 * @implements ProcessorInterface<LotEcritures>
 */
final class LotEcrituresInputProcessor implements ProcessorInterface
{
    public function __construct(
    ) {}

    /**
     * @param LotEcritures $data
     */
    public function process($data, Operation $operation, array $uriVariables = [], array $context = []): LotEcritures
    {
        $data->setId(rand(1,255));
        return $data;
    }
}
