<?php

namespace App\Domain\Model\Additi;

use ApiPlatform\Metadata\ApiProperty;

class Ligne
{
    public function __construct(

        #[ApiProperty(description: "")]
        public string $NumeroDeLigne,

        #[ApiProperty(description: "pour alimenter account=CPTOCECT")]
        public string $CompteComptable,

        #[ApiProperty(description: "pour alimenter debitAmount=MTDOCECT")]
        public string $MontantDebit,

        #[ApiProperty(description: "pour alimenter creditAmount=MTCOCECT")]
        public string $MontantCredit,

        #[ApiProperty(description: "pour alimenter maturityNumber=NECOCECT")]
        public string $NumeroDEcheance,

        #[ApiProperty(description: "pour alimenter transactionFile=DOSOCECT")]
        public string $DossierMouvement,

        #[ApiProperty(description: "pour alimenter cCA=CGROCECT")]
        public string $CGRA,

        #[ApiProperty(description: "pour alimenter cCB=BUDOCECT")]
        public string $CGRB,

        #[ApiProperty(description: "pour alimenter tax=TVAOCECT")]
        public string $CodeTVA,

        #[ApiProperty(description: "pour alimenter transactionDescription=LIMOCECT")]
        public string $Libelle,

        #[ApiProperty(description: "pour alimenter additionalInformation=INFOCECT")]
        public string $LibelleComplementaire,

        #[ApiProperty(description: "pour alimenter externalReference=PIXOCECT")]
        public string $ReferenceExterne,

        #[ApiProperty(description: "pour alimenter transactionNature=NAMOCECT")]
        public string $NatureMouvement,

        #[ApiProperty(description: "pour alimenter transactionKind=GEMOCECT")]
        public string $GenreMouvement,

        #[ApiProperty(description: "pour alimenter transactionRole=ROMOCECT")]
        public string $RoleMouvement,

        protected array $metadonnees=[],
    )
    {

    }

    public function addMetadonnee(Metadonnee $metadonnee): void
    {
        $this->metadonnees[] = $metadonnee;
    }

    public function removeMetadonnee(Metadonnee $metadonnee): void
    {
        if (false !== $key = array_search($metadonnee, $this->metadonnees, true)) {
            unset($this->metadonnees[$key]);
        }
    }

    public function getMetadonnees(): array
    {
        return $this->metadonnees;
    }

}
