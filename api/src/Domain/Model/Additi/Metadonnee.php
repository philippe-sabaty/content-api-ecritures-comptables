<?php

namespace App\Domain\Model\Additi;

use ApiPlatform\Metadata\ApiProperty;

class Metadonnee
{
    public function __construct(
        #[ApiProperty(description: "pour alimenter pour alimenter date1=D01OCECT")]
        public ?string $dateComplementaire1,

        #[ApiProperty(description: "pour alimenter pour alimenter date2=D02OCECT")]
        public ?string $dateComplementaire2,

        #[ApiProperty(description: "pour alimenter transactionParameter1=P1MOCECT")]
        public ?string $complement1,

        #[ApiProperty(description: "pour alimenter transactionParameter2=P2MOCECT")]
        public ?string $complement2,
        public ?string $complement3,
        public ?string $complement4,
        public ?string $complement5,
        public ?string $complement6,
        public ?string $complement7,
        public ?string $complement8,
        public ?string $complement9,

        #[ApiProperty(description: "pour alimenter parameter10=P10OCECT")]
        public ?string $complement10,

        #[ApiProperty(description: "pour alimenter parameter11=P11OCECT")]
        public ?string $complement11,

        #[ApiProperty(description: "pour alimenter parameter12=P12OCECT")]
        public ?string $complement12,
        public ?string $complement13,
        public ?string $complement14,
        public ?string $complement15,
    )
    {

    }
}

/* */
