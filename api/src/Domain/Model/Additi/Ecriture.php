<?php

namespace App\Domain\Model\Additi;

use ApiPlatform\Metadata\ApiProperty;
class Ecriture
{
    private ?int $id = null;

    public function __construct(

        #[ApiProperty(description: "pour alimenter internalNumber=NUIOCECT / C'est un numéro provisoire, commun pour une même écriture et qui ne sera pas conservé lors de l'intégration dans les différentes tables.")]
        public string $NumeroDecriture,

        #[ApiProperty(description: "pour alimenter transactionType=TYPOCECT / doit exister dans GPAR PARAM=TYPMVC.")]
        public string $TypeDecriture,

        #[ApiProperty(description: "pour alimenter entity=ETSOCECT / doit exister dans GETS Intitulé complet INTGTETS.")]
        public string $Etablissement,

        #[ApiProperty(description: "pour alimenter journal=JRNOCECT / doit exister dans GJRN OEJRN.NUMOEJRN.")]
        public string $Journal,

        #[ApiProperty(description: "pour alimenter voucher=PIEOCECT /Si le journal est en numérotation automatique, ne rien renseigner / sinon fournir le numéro de la pièce (libre 10 max).")]
        public string $NumeroPiece,

        #[ApiProperty(description: "pour alimenter transactionType=TYPOCECT / doit exister dans GPAR PARAM=TYPPIE.")]
        public string $TypeDePiece,

        #[ApiProperty(description: "pour alimenter accountingDate=DECOCECT.")]
        public string $DateComptable,

        #[ApiProperty(description: "pour alimenter issueDate=DEMOCECT.")]
        public string $DateEmission,

        #[ApiProperty(description: "pour alimenter endDate=ECHOCECT.")]
        public string $DateEcheance,

        #[ApiProperty(description: "pour alimenter paymentMethod=RGMOCECT / doit exister dans GRGM Intitulé complet INTOERGM.")]
        public string $ModeDePaiement,

        #[ApiProperty(description: "pour alimenter description=LIBOCECT.")]
        public string $Libelle,

        #[ApiProperty(description: "pour alimenter description=LIBOCECT.")]
        public string $LibelleComplementaire,

        #[ApiProperty(description: "pour alimenter additionalDescription=LICOCECT.")]
        public string $LibelleComplementaire2,

        #[ApiProperty(description: "pour alimenter currency=DEVOCECT.")]
        public string $Devise,

        #[ApiProperty(description: "pour alimenter note=BOROCECT.")]
        public string $Bordereau,

        #[ApiProperty(description: "pour alimenter nature=NATOCECT.")]
        public string $NatureEcriture,

        #[ApiProperty(description: "pour alimenter kind=GENOCECT.")]
        public string $GenreEcriture,

        #[ApiProperty(description: "pour alimenter role=ROLOCECT.")]
        public string $RoleEcriture,

        #[ApiProperty(description: "pour alimenter voucherNature=NAPOCECT.")]
        public string $NaturePiece,

        #[ApiProperty(description: "pour alimenter voucherKind=GEPOCECT.")]
        public string $GenrePiece,

        #[ApiProperty(description: "pour alimenter voucherRole=ROPOCECT.")]
        public string $RolePiece,

        #[ApiProperty(description: "pour alimenter entryFile=DOEOCECT.")]
        public string $Dossier,

        #[ApiProperty]
        public string $Createur,

        #[ApiProperty(description: "pour alimenter creationDate=DCROCECT.")]
        public string $DateCreation,

    )
    {

    }

    #[ApiProperty]
    protected array $tiers = [];

    public function addTier(Tiers $tier): void
    {
        $this->tiers[] = $tier;
    }

    public function removeTier(Tiers $tier): void
    {
        if (false !== $key = array_search($tier, $this->tiers, true)) {
            unset($this->tiers[$key]);
        }
    }

    public function getTiers(): array
    {
        return $this->tiers;
    }

    /**
     * @param array<Tiers> $tiers
     * @return void
     */
    public function setTiers(array $tiers): void
    {
        $this->tiers = $tiers;
    }


    /**
     * @var array<Ligne>
     */
    #[ApiProperty]
    protected array $lignes = [];

    public function addLigne(Ligne $ligne): void
    {
        $this->lignes[] = $ligne;
    }

    public function removeLigne(Ligne $ligne): void
    {
        if (false !== $key = array_search($ligne, $this->lignes, true)) {
            unset($this->lignes[$key]);
        }
    }

    public function getLignes(): array
    {
        return $this->lignes;
    }

    /**
     * @var array<Metadonnee>
     */
    #[ApiProperty]
    protected array $metadonnees = [];

    public function addMetadonnee(Metadonnee $metadonnee): void
    {
        $this->metadonnees[] = $metadonnee;
    }

    public function removeMetadonnee(Metadonnee $metadonnee): void
    {
        if (false !== $key = array_search($metadonnee, $this->metadonnees, true)) {
            unset($this->metadonnees[$key]);
        }
    }

    public function getMetadonnees(): array
    {
        return $this->metadonnees;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }


}
