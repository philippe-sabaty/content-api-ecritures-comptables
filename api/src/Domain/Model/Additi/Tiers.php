<?php

namespace App\Domain\Model\Additi;

use ApiPlatform\Metadata\ApiProperty;

class Tiers
{
    public function __construct(

        #[ApiProperty(description: "selon le typeDeTiers / pour alimenter thirdParty=TIEOCECT / concernedThirdParty=TICOCECT / associatedThirdParty=TASOCECT / paymentThirdParty=TIPOCECT")]
        public string $Matricule,

        #[ApiProperty(description: "selon le typeDeTiers / pour alimenter thirdPartyAddress=TIAOCECT / concernedThirdPartyAddress=TCAOCECT / associatedThirdPartyAddress=TAAOCECT / paymentAddress=TAPOCECT")]
        public string $CodeAdresse,

        #[ApiProperty(description: "selon le typeDeTiers / pour alimenter thirdPartyDomicile=TIDOCECT / concernedThirdPartyAddress=TCAOCECT / concernedThirdPartyDomicile=TCDOCECT")]
        public string $CodeDomiciliation,

        #[ApiProperty(description: "")]
        public TypeDeTiers $TypeDeTiers,
    )
    {

    }
}

