<?php

namespace App\Domain\Model\Additi;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Post;
use App\Application\State\LotEcrituresInputProcessor;


#[ApiResource(operations: [
    new Post(processor: LotEcrituresInputProcessor::class)
])]
class LotEcritures
{
    private ?int $id = null;


    public function __construct(

        protected array $ecritures,
    ){ }


    public function addEcriture(Ecriture $ecriture): void
    {
        $this->ecritures[] = $ecriture;
    }

    public function removeEcriture(Ecriture $ecriture): void
    {
        if (false !== $key = array_search($ecriture, $this->ecritures, true)) {
            unset($this->ecritures[$key]);
        }
    }

    /**
     * @return array<Ecriture>
     */
    public function getEcritures(): array
    {
        return $this->ecritures;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }


}
