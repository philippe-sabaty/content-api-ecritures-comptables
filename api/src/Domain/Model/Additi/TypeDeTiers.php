<?php

namespace App\Domain\Model\Additi;

enum TypeDeTiers: string
{
    case FACTURE = 'Facturé';
    case CONCERNE = 'Concerné';
    case ASSOCIE = 'Associé';
    case PAIEMENT = 'Paiement';

}
