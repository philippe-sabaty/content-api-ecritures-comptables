# API Ecritures Comptables


## Livrable 1 : interface coté Additi OK mais bouchonnée.

- créer une Entity Ecriture basée sur la spec schema.draft4.json **(1j)**  
  https://api-platform.com/docs/schema-generator/  

- créer une route POST /ecritures/multiple **(1j)** qui...
  - crée des Entities Ecriture
  - ne fait aucun traitement
  - répond une liste d'Ecriture avec leur etat (inchangé, avec ID factice et status)  
  https://github.com/api-platform/api-platform/issues/294#issuecomment-1559197301  

- implementer la validation des Ecriture et gerer les cas en echec **(1j)**  
-> Quel status HTTP pour un traitement comportant des Ecritures en erreur ?

## Livrable 2 : client de l'API Qualiac
(Ce lot peut etre réalisé en // du Livrable 1)

- crer un service QualiacClient capable de...
    - s'authentifier auprés de l'API Qualiac **(1j)**
    - envoyer en POST une ecriture au format Qualiac (creation_auto=1) **(1j)**
    - recuperer la réponse (sans rien en faire pour l'instant)

# Livrable 3 : client API Qualiac avec gestion d'erreur et logs centralisés

- creer un mecanisme de fallback pour les cas en erreur **(1j)**. Pour chaque ecriture :
    - retry dans le sas (creation_auto=0) dans le cas d'une erreur recuperable (critère ?)
    - erreur 50x sinon
- logguer les evenements erreurs dans un espace mutualisé **(1j)**

# Livrable 4 : route POST /ecritures/multiple entierement effective

- creer un service qui convertisse le format Ecriture Additi vers le format Qualiac et inversement **(1j)**
- connecter les interfaces du Lot 0 au client Qualiac **(1j)**

# Livrable 5 : API Ecritures Comptables déployée en recette
(Ce lot peut etre réalisé dés la fin du Livrable 1)

- identifier la cible de deploiement (GCP/Kube)
- mettre en place un pipeline CI de deploiement vers la cible **(2j)**
- autoriser l'app déployée à joindre l'API Qualiac **(1j)**

# Livrable 6 : API Ecritures Comptables déployée en prod
(Ce lot peut etre réalisé dés la fin du Livrable 1)

- gerer l'authentification des utilisateurs (keycloak) **(1j)**
